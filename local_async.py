import time
import logging
import random
import aiohttp
import asyncio
import pickle
import json
import torch
from torchinfo import summary
from vgg.net import _vgg_custom, vgg_generate_input
from utils import scan_images, torch_load_from_payload
from profiler import test_inference, get_layer_output_bytes


device = torch.device("cpu" if torch.cuda.is_available() else "cpu")
model = _vgg_custom("vgg11", "A", False, pretrained=True, progress=True)
model = model.eval()
model.to(device)

WORKER_COUNT = 3

split_options = [0, 3, 6, 11, 16, 21, 29]
global_split = split_options[1]
global_client_time = []
global_server_time = []
t_client = None
t_server = None
vgg_summary = None

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
)


def estimate_split_latency(split, t_client, t_server, size_list, bandwidth=100):
    t_mobile_acc = sum(t_client[:split])
    t_edge_acc = sum(t_server[split:])
    size = size_list[split]

    t_approx = (
        t_mobile_acc * 2
        + t_edge_acc
        + 0.05 * size * 8 / bandwidth
        + random.random() * 20
    )
    # print(f"{t_mobile_acc}, {t_edge_acc}, {size*8}, {bandwidth}")

    return t_approx


def get_global_split():
    return global_split


async def set_global_split():
    counter = 0
    while True:
        global global_split
        global_split = counter % 30
        counter += 1
        await asyncio.sleep(5)


async def split_update():
    upper_limit = 80
    lower_limit = 60

    global global_split
    while True:
        async with aiohttp.ClientSession() as session:
            async with session.get("http://100.65.131.115:3000/sysload") as resp:
                res = await resp.text()

        split_idx = split_options.index(global_split)

        sysload = json.loads(res)["load"]
        t_approx = estimate_split_latency(
            global_split,
            t_client,
            t_server,
            get_layer_output_bytes(vgg_summary),
            1000000,
        )
        # print(split_idx)
        t_approx_next = estimate_split_latency(
            split_options[(split_idx - 1 if split_idx > 0 else split_idx)],
            t_client,
            t_server,
            get_layer_output_bytes(vgg_summary),
            1000000,
        )
        # print(sysload, t_approx, t_approx_next)

        if sysload > upper_limit:
            # print("Server load spike detected! ")
            logging.warning("Server load spike detected! ")
            split_idx = (
                split_idx + 1 if split_idx < len(split_options) - 1 else split_idx
            )
        else:
            if t_approx_next < t_approx:
                split_idx = split_idx - 1 if split_idx > 0 else split_idx

        global_split = split_options[split_idx]
        logging.info(f"Server CPU load {sysload:.2f}%")
        logging.info(
            f"Estimated: Current layer {global_split} @ {t_approx:.2f}ms, Previous viable layer {split_options[(split_idx - 1 if split_idx > 0 else split_idx)]} @ {t_approx_next:.2f}ms"
        )
        logging.info(f"Next cycle's split point: {global_split}")

        await asyncio.sleep(1)


async def worker(path, endpoint, id=0):
    split = None
    t = time.perf_counter()

    while True:
        if (
            get_global_split() != split
        ):  # Global Split layer has been updated, reconstruct payload
            # global global_client_time
            split = get_global_split()
            # print(f"Reconstructing payload @ {split}")
            # t_start = time.perf_counter()
            with torch.no_grad():
                file_path_list = scan_images(path)
                local_output_list = [
                    {
                        "filename": file,
                        "split": split,
                        "data": model(
                            vgg_generate_input(file).to(device), local=True, split=split
                        ).detach(),
                    }
                    for file in file_path_list
                ]
            payload = pickle.dumps({"net": "vgg", "data": local_output_list})
            # t_client = time.perf_counter() - t_start
            # global_client_time.append(t_client)

        elapsed = asyncio.get_event_loop().time()
        async with aiohttp.ClientSession() as session:
            async with session.post(endpoint, data=payload) as response:
                res = await response.read()
                res_dict = torch_load_from_payload(res)
                # print(res_dict["data"][0]["class"])
                # print(asyncio.get_event_loop().time() - elapsed)

        # global global_server_time
        # global_server_time.append(time.perf_counter() - t)
        logging.info(f"Worker#{id} took {time.perf_counter() - t} seconds")

        await asyncio.sleep(random.random() * 1)
        t = time.perf_counter()


# async def watch_server_load():


async def main(path, split, endpoint):
    print("Starting main coroutine")
    req_count = 0
    uid = 0
    last_finish = time.perf_counter()

    global t_client
    global t_server
    global vgg_summary

    t_client = test_inference(
        model, shape=(1, 3, 224, 224), local=True, repeat_times=10
    )

    t_server = test_inference(
        model, shape=(1, 3, 224, 224), local=False, repeat_times=10
    )
    vgg_summary = summary(
        model,
        input_data=[torch.rand(1, 3, 224, 224), 30, True],
        col_names=(
            "input_size",
            "output_size",
            "mult_adds",
        ),
        verbose=0,
    )

    model.to(device)

    worker_pool = [
        asyncio.create_task(worker(path, endpoint, _)) for _ in range(WORKER_COUNT)
    ]
    split_update_task = asyncio.create_task(split_update())
    worker_pool.append(split_update_task)

    await asyncio.wait(worker_pool)


if __name__ == "__main__":
    main_coroutine = main("images/cat.jpg", 0, "http://100.65.131.115:3000/checkpoint")
    asyncio.run(main_coroutine)
