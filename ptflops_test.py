import torchvision.models as models
import torch
from torchinfo import summary
from ptflops import get_model_complexity_info
from vgg.net import vgg_model

with torch.cuda.device(0):
    # Uncomment to use standard vgg11
    # net = models.vgg11()

    net = vgg_model  # Custom vgg

    def prepare_input(resolution):
        input = torch.FloatTensor(1, *resolution)
        # return dict(x=input)
        return dict(x=input, split=9, local=True)

    macs, params = get_model_complexity_info(
        net,
        (3, 224, 224),
        as_strings=True,
        input_constructor=prepare_input,  # Comment this line to use standard vgg11
        print_per_layer_stat=True,
        verbose=True,
    )
    print("{:<30}  {:<8}".format("Computational complexity: ", macs))
    print("{:<30}  {:<8}".format("Number of parameters: ", params))

    from torchvision.models.detection.ssd import ssd300_vgg16
    report = summary(
        net,
        input_data=[torch.rand(1, 3, 224, 224), 30, True],
        # input_data=[torch.rand(1, 3, 224, 224)],
        col_names=(
            "input_size",
            "output_size",
            # "num_params",
            # "kernel_size",
            "mult_adds",
        ),
    )

    pass
    # ssd = ssd300_vgg16(True)
    # summary(ssd, input_size=(1, 3, 300, 300))
