import time
import requests
import torch
import cv2
import numpy as np
from torchvision.utils import draw_bounding_boxes
from torchvision.transforms.functional import convert_image_dtype
from ssd.net import ssd300_vgg16_custom
from utils import torch_load_from_payload, torch_save_to_bytes

SCORE_THRESHOLD = 0.8
INTERVAL = 10  # Run SSD on every 5 frames

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = ssd300_vgg16_custom(pretrained=True, progress=False)
model.set_ssd_local(True)

model = model.eval()
model.to(device)

image, output = None, None
detect_output = None


def video_detect(video: str, split: int, endpoint: str, show_feature_map=True):
    print("Starting video_detect()")
    t_start = time.clock_gettime(time.CLOCK_REALTIME)

    cap = cv2.VideoCapture(video)
    frame_count = 0

    model.set_backbone_local(local=True)
    model.set_backbone_split(split=split)
    model.set_backbone_endpoint(endpoint_url=endpoint)

    while cap.isOpened():
        frame_count += 1
        ret, frame = cap.read()

        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break

        input_tensor = torch.from_numpy(np.moveaxis(frame, 2, 0))
        batch_int = torch.stack([input_tensor])

        if (
            frame_count == 1 or frame_count % INTERVAL == 0
        ):  # Detect every 5 frames to optimize fps
            t_frame = time.clock_gettime(time.CLOCK_REALTIME)

            input_batch = convert_image_dtype(batch_int, dtype=torch.float)

            input_batch = input_batch.to(device)

            checkpoint_tensor, transformed_input, original_image_sizes = model(
                input_batch
            )

            checkpoint_dict = {
                "split": split,
                "data": checkpoint_tensor,
                "image_sizes": transformed_input.image_sizes,
                "original_image_sizes": original_image_sizes,
            }
            checkpoint_pickle = torch_save_to_bytes(checkpoint_dict)

            res = requests.post(endpoint, data=checkpoint_pickle)

            if res.status_code == requests.codes.ok:
                detect_output = torch_load_from_payload(res.content)[0]

            print(
                f"Inference Time: {time.clock_gettime(time.CLOCK_REALTIME) - t_frame}",
                f"Current FPS: { frame_count / (time.clock_gettime(time.CLOCK_REALTIME) - t_start)}",
                end="\r",
            )

        frame_torch_tensor = batch_int[0]

        image_with_bbox = draw_bounding_boxes(
            frame_torch_tensor,
            boxes=detect_output["boxes"][detect_output["scores"] > SCORE_THRESHOLD],
            width=4,
        )

        if show_feature_map:
            feature_map_tensor = [ch.detach().numpy() for ch in checkpoint_tensor.cpu()[0]][
                :16
            ]

            input_tensor_np = cv2.resize(
                np.moveaxis(input_tensor.detach().numpy(), 0, 2),
                dsize=(600, 600),
                interpolation=cv2.INTER_CUBIC,
            )
            stacked_vis = np.array([])
            for y in range(4):
                row = np.column_stack(feature_map_tensor[y * 4 : y * 4 + 4])
                if (
                    stacked_vis.shape[0] == 0
                ):  # Initiate array with first row, runs only once
                    stacked_vis = row
                stacked_vis = np.row_stack([stacked_vis, row])

            # Resize to 600*600 for window display size
            stacked_vis = cv2.resize(
                stacked_vis, dsize=(600, 600), interpolation=cv2.INTER_CUBIC
            )
            # Stack horizontally beside original image
            stacked_vis = np.hstack(
                [
                    input_tensor_np / 255,
                    cv2.cvtColor(stacked_vis, cv2.COLOR_GRAY2RGB),
                ]
            )
            cv2.imshow("visualized", stacked_vis)

        cv2.imshow("frame", np.moveaxis(image_with_bbox.numpy(), 0, 2))

        if cv2.waitKey(1) == ord("q"):
            break

    cap.release()
    cv2.destroyAllWindows()
    print(
        f"Average FPS: {frame_count / (time.clock_gettime(time.CLOCK_REALTIME) - t_start)}"
    )


if __name__ == "__main__":
    video_detect(
        video="videos/walk.mp4",
        split=5,
        endpoint="http://192.168.31.45:3000/checkpoint-v2",
        show_feature_map=True
    )
