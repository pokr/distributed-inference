import torch
from torchvision.models import VGG
import torch.nn as nn
from torchvision.models.vgg import make_layers, cfgs
from typing import Any


class VGGCustom(VGG):
    def __init__(
        self,
        features: nn.Module,
        num_classes: int = 1000,
        init_weights: bool = True,
        dropout: float = 0.5,
    ) -> None:
        super().__init__(features, num_classes, init_weights, dropout)
        self.benchmark = []

    def convert_module_list(self):
        self.module_list = nn.ModuleList()
        for layer_f in self.features:
            self.module_list.append(layer_f)
        self.module_list.append(self.avgpool)
        self.module_list.append(nn.Flatten(start_dim=1))
        for layer_c in self.classifier:
            self.module_list.append(layer_c)

        del self.features
        del self.avgpool
        del self.classifier

    def forward(self, x: torch.Tensor, split: int, local: bool) -> torch.Tensor:
        if local:  # Local
            modules = range(0, split)
        else:
            modules = range(split, len(self.module_list))

        benchmark_per_input = []
        if False:
            for i in modules:
                starter = torch.cuda.Event(enable_timing=True)
                ender = torch.cuda.Event(enable_timing=True)

                starter.record()
                x = self.module_list[i](x)
                ender.record()

                torch.cuda.synchronize()
                time_cost = starter.elapsed_time(ender)

                benchmark_per_input.append(
                    {
                        "No": i,
                        "Local": local,
                        "Module": str(self.module_list[i]),
                        "time": time_cost,
                        "Shape": str(x.shape),
                        "Size": f"{x.nelement() * x.element_size() / 1000} KB",
                    }
                )
        else:
            import time

            for i in modules:
                starter = time.clock_gettime(time.CLOCK_REALTIME)
                x = self.module_list[i](x)
                ender = time.clock_gettime(time.CLOCK_REALTIME)

                time_cost = (ender - starter) * 1000

                benchmark_per_input.append(
                    {
                        "No": i,
                        "Local": local,
                        "Module": str(self.module_list[i]),
                        "time": time_cost,
                        "Shape": str(x.shape),
                        "Size": f"{x.nelement() * x.element_size() / 1000} KB",
                    }
                )

        self.benchmark.append(benchmark_per_input)

        return x


def _vgg_custom(
    arch: str,
    cfg: str,
    batch_norm: bool,
    pretrained: bool,
    progress: bool,
    **kwargs: Any,
) -> VGGCustom:
    if pretrained:
        kwargs["init_weights"] = False
    model = VGGCustom(make_layers(cfgs[cfg], batch_norm=batch_norm), **kwargs)
    if pretrained:
        state_dict = torch.load("checkpoints/vgg11-8a719046.pth")
        model.load_state_dict(state_dict)
        model.convert_module_list()

    return model


def vgg_generate_input(filename):
    from PIL import Image
    from torchvision import transforms

    input_image = Image.open(filename)
    preprocess = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    input_tensor = preprocess(input_image)
    input_batch = input_tensor.unsqueeze(0)
    return input_batch.to("cpu")


def vgg_run_inference(input_batch, local: bool, split: int):
    # move the input and model to GPU for speed if available
    # if torch.cuda.is_available():
    #     input_batch = input_batch.to("cuda")
    #     vgg_model.to("cuda")
    vgg_model.to("cpu")

    with torch.no_grad():
        output = vgg_model(input_batch, local=local, split=split)

    return output


def vgg_classify(output):
    # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
    probabilities = torch.nn.functional.softmax(output[0], dim=0)
    # print(probabilities)

    # Read the categories
    with open("vgg/imagenet_classes.txt", "r") as f:
        categories = [s.strip() for s in f.readlines()]
    # Show top categories per image
    top5_prob, top5_catid = torch.topk(probabilities, 5)

    result = {}
    for i in range(top5_prob.size(0)):
        result[categories[top5_catid[i]]] = top5_prob[i].item()

    return result


vgg_model = _vgg_custom("vgg11", "A", False, pretrained=True, progress=True)
