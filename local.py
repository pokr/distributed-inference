import json, pickle, time
import requests
from utils import get_net_funcs, scan_images, torch_load_from_payload


def local_init(input_path: str, split: int, net: str, endpoint: str):
    t_local_start = time.clock_gettime(time.CLOCK_REALTIME)
    # 1. Get inference & classifying functions
    (net_inference_run, net_generate_input, c, net_model) = get_net_funcs(net)

    # 2. Generate (tensor) input data from file path
    file_list = scan_images(input_path)
    input_data_list = [
        {
            "filename": file,
            "data": net_generate_input(file),
        }
        for file in file_list
    ]

    # 3. Run local inference and get split output
    local_split_output = []
    for i in input_data_list:
        inference_result = net_inference_run(
            i["data"], local=True, split=split
        ).detach()

        local_split_output.append(
            {
                "filename": i["filename"],
                "data": inference_result,
            }
        )

    if split == len(net_model.module_list):
        t_local_inference = 0
        for per_image in net_model.benchmark:
            print("Local inference latency: ", sum([b["time"] for b in per_image]))
        return

    # print(len(pickled_output) / 100000, "M")
    # print(f"Pickle payload size: {len(pickled_output)}")

    # 4. POST split output data to server
    data_post = {"net": net, "data": []}
    for o in local_split_output:
        data_post["data"].append(
            {
                "filename": o["filename"],
                "split": split,
                "data": o["data"],
            }
        )

    b_data_post_pickle = pickle.dumps(data_post)
    print(f"Pickle payload size: {len(b_data_post_pickle)}")

    t_post_start = time.clock_gettime(time.CLOCK_REALTIME)
    res_post = requests.post(endpoint, data=b_data_post_pickle)

    # Get HTTP response
    if res_post.status_code == requests.codes.ok:
        print("SUCCESS")
        t_post_got_response = time.clock_gettime(time.CLOCK_REALTIME)

        res_unpacked = torch_load_from_payload(res_post.content)
        edge_output = res_unpacked["data"]
        t_transmit = res_unpacked["time_transmit"] * 1000
        t_gen_response = res_unpacked["time_gen_response"] * 1000

        for img in edge_output:
            print(f"File: {img['filename']}")
            print(f"Classes: {img['class']}")

        # with open("benchmark_result.json", "w") as f:
        full_benchmark = [
            (net_model.benchmark[i] + edge_output[i]["bench"])
            for i in range(len(edge_output))
        ]
        net_model.benchmark = []

        t_round_trip = res_post.elapsed.total_seconds() * 1000
        t_server_inference = 0
        t_local_inference = 0
        for input in full_benchmark:
            for module in input[:split]:
                t_local_inference += module["time"]
            for module in input[split:]:
                t_server_inference += module["time"]

        # t_transmit = t_round_trip - t_gen_response
        t_server_warmup = t_gen_response - t_server_inference
        t_full = (t_post_got_response - t_local_start) * 1000
        t_local = (t_post_start - t_local_start) * 1000
        t_local_warmup = t_local - t_local_inference

        bench_log = {
            "time_full": t_full,
            "time_local": t_local,
            "time_local_warmup": t_local_warmup,
            "time_round_trip": t_round_trip,
            "time_transmit": t_transmit,
            "time_server": t_gen_response,
            "time_inference": t_server_inference,
            "time_local_inference": t_local_inference,
            "time_server_warmup": t_server_warmup,
            "split": split,
            "benchmark": full_benchmark,
        }

        print(json.dumps(bench_log))
        return bench_log
    else:
        print("FAILED")
