import argparse

Options = None
DEFAULT_ENDPOINT = "http://127.0.0.1:3000/checkpoint"


def parse_args():
    parser = argparse.ArgumentParser(description="Run distributed inference")
    parser.add_argument(
        "input",
        metavar="INPUT_PATH",
        type=str,
        nargs=1,
        help="Path to the input image",
    )
    parser.add_argument(
        "--type",
        type=str,
        required=True,
        help="Type of input (image/video)",
    )
    parser.add_argument(
        "--split",
        type=int,
        required=True,
        help="The layer to split the network on",
    )
    parser.add_argument(
        "--net",
        required=False,
        help="Specifies the network to be used (e.g. vgg); See supported networks in README.md",
    )
    parser.add_argument(
        "--endpoint",
        type=str,
        required=False,
        default=DEFAULT_ENDPOINT,
        help="Server API endpoint address (e.g. http://127.0.0.1:3000/checkpoint)",
    )
    parser.add_argument("--scenario", type=str, required=False, default="")
    parser.add_argument("--device", type=str, required=False, default="")

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    Options = parse_args()
    print(Options)

    if Options.type == "image":
        from local import local_init

        local_init(
            input_path=Options.input[0],
            split=Options.split,
            net=Options.net,
            endpoint=Options.endpoint,
        )

    elif Options.type == "video":
        from video import video_detect

        video_detect(
            video=Options.input[0],
            split=Options.split,
            endpoint=Options.endpoint,
            show_feature_map=False,
        )

    elif Options.type == "test":
        import json
        from local import local_init

        logs = []
        local_init(
            input_path=Options.input[0],
            split=0,
            net=Options.net,
            endpoint=Options.endpoint,
        )

        for i in range(Options.split):
            logs.append(
                local_init(
                    input_path=Options.input[0],
                    split=i,
                    net=Options.net,
                    endpoint=Options.endpoint,
                )
            )

        with open(
            f"log/bench_all_layers_{Options.device}_{Options.scenario}.json", "w"
        ) as f:
            json.dump(logs, f)
