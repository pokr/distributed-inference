# 神经网络分割推理Demo

在PyTorch框架下，神经网络以`nn.Module`（模块）为最小单位组合而成。在运行训练或推理时，Torch会调用各个模块的`forward()`函数进行前向传播。由于我们仅希望在推理过程中应用网络分割，只需要在前向传播环节介入并令一部分算力卸载至服务端即可。

本例中实现分割的网络包括VGG11和AlexNet，此外还实现了用VGG16作为特征提取网络的SSD目标检测算法的分割运行。

上述的网络、算法均采用了[torchvision](https://pytorch.org/vision/stable/index.html)项目的网络定义与预训练权重，在继承了官方网络的类（Class）之后定义一个`convert_module_list()`函数，将官方网络定义的所有模块按顺序铺平，排列为一个`nn.ModuleList`；最后重定义`forward()`等函数，令该`ModuleList`顺序执行，并在指定的分割点将该层模块输出的张量发送至服务端，完成后续的推理。

假设在S层分割`Net`以运行图像的识别类任务，具体的流程如下：

1. 客户端将输入的图像预处理并送入`Net`
2. `Net`内部调用`forward()`函数遍历`ModuleList`进行前向传播，直到第S个模块停止循环
3. 客户端将第S-1个模块的输出张量编码为`pickle`，通过HTTP POST请求发送到服务端
4. 服务端解码`pickle`获得客户端最后输出的张量，送入服务端本地的`Net`实例
5. 服务端本地的`Net`实例调用`forward()`函数完成第S个至最后一个模块的前向传播
6. 根据应用需求，将最终输出的张量送入分类器进行判断或直接返回客户端。

需要注意的是，PyTorch中网络的实现与严格意义上的神经网络“层”并非严格对应，如ReLU函数等激活函数都单独作为一个Module。


<img src="README.assets/image-20220327161610685.png" alt="image-20220327161610685" style="zoom:50%;" />

## 安装

推荐使用`conda`进行依赖管理。

具体的版本见`requirements.txt`

- Python 3.7
- torch
- torchvision
- opencv_python
- numpy
- Flask
- requests

## 用法

首先启动Flask服务端（需要安装flask）

```powershell
# Windows
> $env:FLASK_APP = "server"
> flask run --port 3000
```

```bash
# Unix-like
$ env FLASK_APP=server flask run --port 3000
```

```bash
# 运行图像识别，输入images目录下全部图片，于第8个模块处分割，使用VGGNet（vgg11）网络，给定服务端接口
$ python run.py --image images --split 8 --net vgg --endpoint http://127.0.0.1:3000/checkpoint

# 运行视频目标检测，输入video/walk.mp4，于第16个模块处分割，缺省使用SSD算法，给定服务端接口
$ python run.py videos/walk.mp4 --type video --split 16 --endpoint http://127.0.0.1:3000/checkpoint-v2
```

Docker: 

*暂未维护，可能无法正常工作*

```bash
# Build: 
$ docker build -t torch-test .

# Run: 
# Corresponding weights must exist in ./checkpoints
$ docker run --rm -it -v "$(realpath ./checkpoints):/app/checkpoints" torch-test
```
