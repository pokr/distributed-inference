import pickle
import torch
from torch import tensor
from utils import get_net_funcs
from ssd.net import ssd300_vgg16_custom
from vgg.net import _vgg_custom, vgg_classify

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model_vgg = _vgg_custom("vgg11", "A", False, pretrained=True, progress=True)
model_vgg = model_vgg.eval()
model_vgg.to(device)

model_ssd = ssd300_vgg16_custom(pretrained=True, progress=False)
model_ssd = model_ssd.eval()
model_ssd.to(device)



def run_v2(checkpoint_dict):
    split = checkpoint_dict["split"]
    data = checkpoint_dict["data"]
    image_sizes = checkpoint_dict["image_sizes"]
    original_image_sizes = checkpoint_dict["original_image_sizes"]

    model_ssd.set_ssd_local(False)
    model_ssd.set_backbone_local(False)
    model_ssd.set_backbone_split(split=split)
    model_ssd.set_image_sizes(image_sizes, original_image_sizes)

    with torch.no_grad():
        res = model_ssd(data)

    return res


def run(checkpoint_dict):
    # Parse data
    net_type = checkpoint_dict["net"]
    split_input_list = checkpoint_dict["data"]

    # (net_inference_run, g, net_classify, net_model) = get_net_funcs(net_type)

    output_list = []
    with torch.no_grad(): 
        for i in split_input_list:
            output = model_vgg(i["data"].to(device), local=False, split=i["split"])
            # bench = net_model.benchmark[-1]
            class_result = list(vgg_classify(output).keys())[:3]

            output_list.append(
                {
                    "filename": i["filename"],
                    # "data": output,
                    # "bench": bench,
                    "class": class_result,
                }
            )
    return output_list
