import numpy as np
import matplotlib.pyplot as plt
import torch
import torchvision
from torchvision.io import read_image
from torchvision import transforms
from torchvision.utils import draw_bounding_boxes
from torchvision.transforms.functional import convert_image_dtype
import torchvision.transforms.functional as F
from PIL import Image, ImageDraw
from .utils import show

plt.rcParams["savefig.bbox"] = "tight"



# filename = "./images/labrador.jpg"
# filename = "./images/cat.jpg"
filename = "images/truck.jpg"

input_image = Image.open(filename).convert("RGB")

input_tensor = read_image(filename)
batch_int = torch.stack([input_tensor])
input_batch = convert_image_dtype(batch_int, dtype=torch.float)

transform_seq = transforms.Compose(
    [
        # transforms.Resize((300, 300)),
        transforms.ToTensor(),  # range [0, 255] -> [0.0,1.0]
    ]
)

transformed_image = transform_seq(input_image)

input_batch_2 = torch.stack([transformed_image])
input_batch_2 = [transformed_image]
# # x = [torch.rand(3, 300, 300), torch.rand(3, 500, 400)]

# model = torchvision.models.detection.ssd300_vgg16(pretrained=True, progress=False)
# model = model.eval()

model = torchvision.models.detection.ssd300_vgg16(pretrained=True, progress=False)
model = model.eval()

# outputs = model(input_batch)
outputs = model(input_batch_2)

# print(pred)

score_threshold = 0.8
dogs_with_boxes = [
    draw_bounding_boxes(
        dog_int, boxes=output["boxes"][output["scores"] > score_threshold], width=4
    )
    for dog_int, output in zip(batch_int, outputs)
]
show(dogs_with_boxes)

top3_box = outputs[0]["boxes"][:3]

# resized = input_image.resize((300, 300))
output_image = ImageDraw.Draw(input_image)
for box in top3_box:
    [x1, y1, x2, y2] = box.tolist()
    shape = [(x1, y1), (x2, y2)]
    output_image.rectangle(shape, outline="red")

input_image.show()
