import types
import warnings
import requests
from collections import OrderedDict
from typing import Any, Dict, List, Optional, Tuple

import torch
from torch import nn, Tensor
import torch.nn.functional as F
from torchvision.ops import boxes as box_ops
from torchvision.models.detection import SSD
from torchvision.models import vgg

from torchvision.models.detection.ssd import _vgg_extractor
from torchvision.models.detection.backbone_utils import _validate_trainable_layers

from torchvision.models.detection.anchor_utils import DefaultBoxGenerator
from utils import torch_load_from_payload, torch_save_to_bytes


class SSDCustom(SSD):
    def __init__(
        self,
        backbone: nn.Module,
        anchor_generator: DefaultBoxGenerator,
        size: Tuple[int, int],
        num_classes: int,
        image_mean: Optional[List[float]] = None,
        image_std: Optional[List[float]] = None,
        head: Optional[nn.Module] = None,
        score_thresh: float = 0.01,
        nms_thresh: float = 0.45,
        detections_per_img: int = 200,
        iou_thresh: float = 0.5,
        topk_candidates: int = 400,
        positive_fraction: float = 0.25,
    ):
        super().__init__(
            backbone,
            anchor_generator,
            size,
            num_classes,
            image_mean,
            image_std,
            head,
            score_thresh,
            nms_thresh,
            detections_per_img,
            iou_thresh,
            topk_candidates,
            positive_fraction,
        )
        self.local = True

    def set_backbone_local(self, local: bool):
        self.backbone.local = local

    def set_backbone_split(self, split: int):
        self.backbone.split = split

    def set_backbone_endpoint(self, endpoint_url: str):
        self.backbone.endpoint = endpoint_url

    def set_ssd_local(self, local: bool):
        self.local = local

    def set_image_sizes(self, image_sizes, original_image_sizes):
        self.image_sizes = image_sizes
        self.original_image_sizes = original_image_sizes

    def forward(
        self, input, targets: Optional[List[Dict[str, Tensor]]] = None
    ) -> Tuple[Dict[str, Tensor], List[Dict[str, Tensor]]]:
        # get the original image sizes
        if self.local:
            self.set_backbone_local(True)  # Failsafe

            original_image_sizes: List[Tuple[int, int]] = []
            for img in input:
                val = img.shape[-2:]
                assert len(val) == 2
                original_image_sizes.append((val[0], val[1]))

            # transform the input
            input, targets = self.transform(input, targets)

            # Check for degenerate boxes
            if targets is not None:
                for target_idx, target in enumerate(targets):
                    boxes = target["boxes"]
                    degenerate_boxes = boxes[:, 2:] <= boxes[:, :2]
                    if degenerate_boxes.any():
                        bb_idx = torch.where(degenerate_boxes.any(dim=1))[0][0]
                        degen_bb: List[float] = boxes[bb_idx].tolist()
                        raise ValueError(
                            "All bounding boxes should have positive height and width."
                            f" Found invalid box {degen_bb} for target at index {target_idx}."
                        )

            backbone_checkpoint = self.backbone(input.tensors)
            return backbone_checkpoint, input, original_image_sizes

        else:
            self.set_backbone_local(False)  # Failsafe

            original_image_sizes = self.original_image_sizes
            input_dummy = torch.rand(
                (
                    1,
                    3,
                )
                + tuple(self.image_sizes[0])
            )
            input_dummy.tensors = input_dummy
            input_dummy.image_sizes = self.image_sizes
            print(f"Size: {input_dummy.shape}")

            features = self.backbone(input)
            if isinstance(features, torch.Tensor):
                features = OrderedDict([("0", features)])

            features = list(features.values())
            # compute the ssd heads outputs using the features
            head_outputs = self.head(features)

            # create the set of anchors
            anchors = self.anchor_generator(input_dummy, features)

            losses = {}
            detections: List[Dict[str, Tensor]] = []

            detections = self.postprocess_detections(
                head_outputs, anchors, input_dummy.image_sizes
            )
            detections = self.transform.postprocess(
                detections, input_dummy.image_sizes, original_image_sizes
            )

            return self.eager_outputs(losses, detections)


def convert_module_list(self):
    """
    Since SSD extracts outputs from multiple stages of network inference,
    here we only do splitting on the features part, which includes modules
    from input to Conv4_3 in the original VGG16 network.
    While it is possible to do splitting on the later layers, it would
    involve more complicated communication procedure between client/server,
    so it's not our priority.
    """
    self.module_list = nn.ModuleList()
    for l in self.features:
        self.module_list.append(l)


def extractor_forward(self, x: Tensor) -> Dict[str, Tensor]:
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    x = x.to(device)

    if self.local:  # Local
        for i in range(0, self.split):
            # print(f"Running local layer {i}")
            x = self.module_list[i](x)

        return x

    if not self.local:  # Edge
        for i in range(self.split, len(self.module_list)):
            # print(f"Running local layer {i}")
            x = self.module_list[i](x)
        # L2 regularization + Rescaling of 1st block's feature map
        rescaled = self.scale_weight.view(1, -1, 1, 1) * F.normalize(x)
        output = [rescaled]

        # Calculating Feature maps for the rest blocks
        for block in self.extra:
            x = block(x)
            output.append(x)

        return OrderedDict([(str(i), v) for i, v in enumerate(output)])


def ssd300_vgg16_custom(
    pretrained: bool = False,
    progress: bool = True,
    num_classes: int = 91,
    pretrained_backbone: bool = True,
    trainable_backbone_layers: Optional[int] = None,
    **kwargs: Any,
):
    if "size" in kwargs:
        warnings.warn("The size of the model is already fixed; ignoring the argument.")

    trainable_backbone_layers = _validate_trainable_layers(
        pretrained or pretrained_backbone, trainable_backbone_layers, 5, 4
    )

    if pretrained:
        # no need to download the backbone if pretrained is set
        pretrained_backbone = False

    backbone = vgg.vgg16(
        pretrained=False, progress=progress
    )  # Initiate backbone vgg16 network

    backbone = _vgg_extractor(
        backbone, False, trainable_backbone_layers
    )  # Convert to SSD's extractor to provide multiple feature maps

    anchor_generator = DefaultBoxGenerator(
        [[2], [2, 3], [2, 3], [2, 3], [2], [2]],
        scales=[0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05],
        steps=[8, 16, 32, 64, 100, 300],
    )

    defaults = {
        # Rescale the input in a way compatible to the backbone
        "image_mean": [0.48235, 0.45882, 0.40784],
        "image_std": [
            1.0 / 255.0,
            1.0 / 255.0,
            1.0 / 255.0,
        ],  # undo the 0-1 scaling of toTensor
    }
    kwargs = {**defaults, **kwargs}

    print("Creating SSDCustom instance")
    model = SSDCustom(
        backbone,
        anchor_generator,
        (300, 300),
        num_classes,
        **kwargs,
    )
    ssd_state_dict = torch.load("checkpoints/ssd300_vgg16_coco-b556d3b4.pth")
    print("Loading model weights")
    model.load_state_dict(ssd_state_dict)

    model.backbone.convert_module_list = types.MethodType(convert_module_list, backbone)
    model.backbone.convert_module_list()

    model.backbone.forward = types.MethodType(extractor_forward, backbone)

    model.set_backbone_local(local=True)  # Default to local
    model.set_backbone_split(split=23) # Default to 23 (last)
    return model
