#!/usr/bin/bash

DEVICE=$1

sudo tc qdisc replace dev wlan0 root netem delay 2ms rate 100Mbit loss 0.2%
# SPLIT=$(python profiler.py | tail -n 1 | awk '{print $5}')
# echo split=$SPLIT
# python run.py --type image --split $SPLIT --net vgg --endpoint http://192.168.31.45:3000/checkpoint images/cat.jpg > log/bench_wifi.txt
# tail -n 1 log/bench_wifi.txt | jq . > log/bench_wifi.json
python run.py --type test --split 30 --net vgg --device $DEVICE --scenario wifi --endpoint http://192.168.31.45:3000/checkpoint images/cat.jpg

sudo tc qdisc replace dev wlan0 root netem delay 10ms rate 50Mbit loss 0.2%
# SPLIT=$(python profiler.py | tail -n 1 | awk '{print $5}')
# echo split=$SPLIT
# python run.py --type image --split $SPLIT --net vgg --endpoint http://192.168.31.45:3000/checkpoint images/cat.jpg > log/bench_5g.txt
# tail -n 1 log/bench_5g.txt | jq . > log/bench_5g.json
python run.py --type test --split 30 --net vgg --device $DEVICE --scenario 5g --endpoint http://192.168.31.45:3000/checkpoint images/cat.jpg

sudo tc qdisc replace dev wlan0 root netem delay 50ms rate 20Mbit loss 0.2%
# SPLIT=$(python profiler.py | tail -n 1 | awk '{print $5}')
# echo split=$SPLIT
# python run.py --type image --split $SPLIT --net vgg --endpoint http://192.168.31.45:3000/checkpoint images/cat.jpg > log/bench_4g.txt
# tail -n 1 log/bench_4g.txt | jq . > log/bench_4g.json
python run.py --type test --split 30 --net vgg --device $DEVICE --scenario 4g --endpoint http://192.168.31.45:3000/checkpoint images/cat.jpg


sudo tc qdisc del dev wlan0 root netem
