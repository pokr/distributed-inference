import subprocess, shlex
import time
import pickle
import json
from flask import Flask, request, Response
from edge import run, run_v2
from utils import torch_load_from_payload, torch_save_to_bytes

app = Flask(__name__)


@app.route("/sysload", methods=["GET"])
def sysload():
    sh = shlex.split("sar -u ALL 3 1")
    p = subprocess.run(sh, capture_output=True, text=True)
    load_5sec = ""
    for line in p.stdout.splitlines():
        load_5sec += line

    load = 100 - float(load_5sec.split(" ")[-1])

    return Response(response=json.dumps({"load": load}), status=200)


@app.route("/checkpoint", methods=["POST"])
def upload():
    t_header = time.clock_gettime(time.CLOCK_REALTIME)
    data = request.get_data()
    t_data_ready = time.clock_gettime(time.CLOCK_REALTIME)
    checkpoint_pickle = pickle.loads(data)

    output = run(checkpoint_pickle)
    t_inference_finish = time.clock_gettime(time.CLOCK_REALTIME)

    t_transmit = t_data_ready - t_header
    t_gen_response = t_inference_finish - t_data_ready

    b_buffer = torch_save_to_bytes(
        {
            "data": output,
            "time_transmit": t_transmit,
            "time_gen_response": t_gen_response,
        }
    )

    return Response(response=b_buffer, status=200)


@app.route("/checkpoint-v2", methods=["POST"])
def upload_v2():
    data = request.get_data()
    checkpoint_dict = torch_load_from_payload(data)

    b_buffer = torch_save_to_bytes(run_v2(checkpoint_dict))

    return Response(response=b_buffer, status=200)
