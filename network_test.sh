if [[ $1 == "ping" ]]; then
    ping -c 4 $2 | tail -1 | awk '{print $4}' | cut -d '/' -f 2
elif [[ $1 == "bandwidth" ]]; then
    iperf3 -c 192.168.31.45 -f K | tail -3 | head -1 | awk '{print $7}'
else
    echo >&2 "Please specify a task"
fi
