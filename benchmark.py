import torch
from torch.utils import benchmark
from video import video_detect

image_inference_stmt = "local_init('images',6,'vgg','http://127.0.0.1:3000/checkpoint')"
image_inference_setup = "from local import local_init"

dict_res = {}

for s in range(21):
    t_image_inference = benchmark.Timer(
        stmt=f"local_init('images',{s},'vgg','http://127.0.0.1:3000/checkpoint')",
        setup=image_inference_setup,
    )
    res_image_inference = t_image_inference.timeit(5)
    dict_res[str(s)] = res_image_inference


print(dict_res)
# with open("image_benchmark.txt", "w") as f:
#     f.write(res_image_inference)

pass
