import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import json

plt.rcParams["figure.autolayout"] = True
font = {"family": "normal", "weight": "regular", "size": 18}
matplotlib.rc("font", **font)


def plot_all_splits(path: str, device: str = "", total_splits=30):
    with open(path, "r") as f:
        benchmark = json.load(f)

    layers = np.arange(len(benchmark[0]["benchmark"][0]))
    t_inference = [split["time_full"] for split in benchmark]

    width = 0.4

    plt.figure(figsize=[12, 6])
    plt.bar(
        layers,
        t_inference,
        width=width,
        color="blue",
        alpha=0.6,
        label=device,
        tick_label=layers,
    )

    plt.legend()
    # plt.axis([-1, len(layers), 0, 1.1 * max(np.maximum(t_mobile, t_edge))])
    plt.grid(True)
    plt.xlabel("Layer")
    plt.ylabel("Inference Time (ms, Log Scale)")
    plt.yscale("log")

    plt.savefig(f"./plots/plot-inference-splits-{device}.png")
    # plt.show()


def plot_all_splits_multi(paths, scenarios, device=""):
    t_list = []
    for p in paths:
        with open(p, "r") as f:
            benchmark = json.load(f)
            t_inference = [split["time_full"] for split in benchmark]
            t_list.append(t_inference)

    layers = np.arange(len(t_list[0]))
    width = 0.25

    plt.figure(figsize=[12, 6])

    colors = ["blue", "red", "green"]
    for s in range(len(t_list)):
        plt.bar(
            layers + s * width,
            t_list[s],
            width=width,
            color=colors[s % 3],
            alpha=0.6,
            label=scenarios[s],
            tick_label=layers,
        )

    plt.legend()
    # plt.axis([-1, len(layers), 0, 1.1 * max(np.maximum(t_mobile, t_edge))])
    plt.grid(True)
    plt.xlabel("Layer")
    plt.ylabel("Inference Time (ms, Log Scale)")
    plt.yscale("log")

    plt.savefig(f"./plots/plot-inference-splits-{device}-{scenarios}.png")


def plot_video(path, scenario=""):
    with open(path) as f:
        buf = f.readlines()
        buf = [float(line.rstrip()) for line in buf if line != "\n"]

    layers = np.arange(len(buf))

    width = 0.25
    backbone_layers = np.arange(len(buf))

    plt.figure(figsize=[12, 6])
    plt.bar(
        backbone_layers,
        buf,
        width=width,
        color="blue",
        alpha=0.6,
        label="Laptop",
        tick_label=backbone_layers,
    )
    plt.legend()
    plt.axis([-1, len(layers), 0, 1.1 * max(buf)])
    plt.grid(True)
    plt.xlabel("Layer")
    plt.ylabel("Video Detection FPS (ms)")
    # plt.yscale("log")

    plt.savefig(f"./plots/plot-inference-video-laptop{scenario}.png")


def plot_video_multi(paths, scenarios, device=""):
    frame_list = []
    for p in paths:
        with open(p) as f:
            buf = f.readlines()
            buf = [float(line.rstrip()) for line in buf if line != "\n"]
            frame_list.append(buf)

    layers = np.arange(len(frame_list[0]))
    width = 0.25

    plt.figure(figsize=[12, 6])

    colors = ["blue", "red", "green", "fuchsia"]
    for s in range(len(frame_list)):
        plt.bar(
            layers + s * width,
            frame_list[s],
            width=width,
            color=colors[s],
            alpha=0.6,
            label=scenarios[s],
            tick_label=layers,
        )

    plt.legend(prop={"size": 10})
    # plt.axis([-1, len(layers), 0, 1.1 * max(np.maximum(t_mobile, t_edge))])
    plt.grid(True)
    plt.xlabel("Layer")
    plt.ylabel("Video Detection FPS (ms)")
    # plt.yscale("log")

    plt.savefig(f"./plots/plot-inference-video-{device}-{scenarios}.png")


if __name__ == "__main__":
    plot_all_splits(
        "log/bench_all_layers_laptop-stressed-40_4g.json", "laptop-stressed-40"
    )
    plot_all_splits_multi(
        [
            "log/bench_all_layers_laptop-stressed-40_4g.json",
            "log/bench_all_layers_laptop-stressed-60_4g.json",
            "log/bench_all_layers_laptop-stressed-80_4g.json",
        ],
        ["40", "60", "80"],
        device="laptop-4g",
    )
    plot_all_splits_multi(
        [
            "log/bench_all_layers_laptop-stressed-80_4g.json",
            "log/bench_all_layers_laptop-stressed-80_5g.json",
            "log/bench_all_layers_laptop-stressed-80_wifi.json",
        ],
        ["4g", "5g", "wifi"],
        device="laptop-stress-80",
    )
    plot_all_splits_multi(
        [
            "log/bench_all_layers_laptop-stressed-40_wifi.json",
            "log/bench_all_layers_laptop-stressed-60_wifi.json",
            "log/bench_all_layers_laptop-stressed-80_wifi.json",
        ],
        ["4g", "5g", "wifi"],
        device="laptop-wifi",
    )

    plot_video("log/bench_video_laptop_lan_stress_0.txt", scenario="stress_0")
    plot_video_multi(
        [
            "log/bench_video_laptop_lan_stress_0.txt",
            # "log/bench_video_laptop_lan_stress_40.txt",
            # "log/bench_video_laptop_lan_stress_60.txt",
            "log/bench_video_laptop_lan_stress_80.txt",
        ],
        [
            "0",
            # "40",
            # "60",
            "80",
        ],
        device="laptop-stress",
    )
    plot_video("./log/bench_video_laptop_lan.txt", scenario="lan")
    plot_video("./log/bench_video_laptop.txt")
