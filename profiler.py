import subprocess, shlex
from typing import List
import datetime
import argparse
import requests
import pickle
import json
import numpy as np
import torch
import torch.nn as nn
from torchinfo import summary, ModelStatistics
from vgg.net import _vgg_custom
from utils import torch_load_from_payload

HOST = "192.168.31.45"
INPUT_SIZE_AVG = 300000
SIZE_FACTOR = 8

device = torch.device("cpu" if torch.cuda.is_available() else "cpu")

vgg_model = _vgg_custom("vgg11", "A", False, pretrained=True, progress=True)


def summary_vgg11() -> ModelStatistics:
    return summary(
        vgg_model,
        input_data=[torch.rand(1, 3, 224, 224), 30, True],
        col_names=(
            "input_size",
            "output_size",
            "mult_adds",
        ),
    )


def test_latency(host, dry_run=False) -> float:
    # Return ICMP ping latency in ms
    # ping -c 4 $HOST | tail -1 | awk '{print $4}'
    if dry_run:
        return 120

    args = shlex.split(f"bash network_test.sh ping {host}")
    proc = subprocess.run(args, capture_output=True)

    ping_rtt_avg = float(proc.stdout.decode("utf-8").strip("\n"))
    return ping_rtt_avg


def test_bandwidth(host, dry_run=False) -> float:
    # Assumes an iperf3 instance is running on server host's 5201 (default) port

    if dry_run:
        return 500000

    # Return bandwidth in Bytes per second
    args = shlex.split(f"bash network_test.sh bandwidth {host}")
    proc = subprocess.run(args, capture_output=True)

    bandwidth_KB = float(proc.stdout.decode("utf-8").strip("\n"))
    bandwidth = bandwidth_KB * 1000
    return bandwidth


def test_inference(
    net: nn.Module, shape: tuple, local: bool, repeat_times: int = 10
) -> List[float]:
    net.to("cpu")
    split = len(net.module_list) if local else 0
    latency_avg, latency_per_run = [], []

    for run in range(repeat_times):
        input_dummy = torch.rand(shape).to("cpu")

        if not local:
            data_post = {"net": "vgg", "data": []}
            data_post["data"].append(
                {
                    "filename": "dummy",
                    "split": split,
                    "data": input_dummy,
                }
            )

            res_post = requests.post(
                f"http://{HOST}:3000/checkpoint", data=pickle.dumps(data_post)
            )

            if res_post.status_code == requests.codes.ok:
                res_unpacked = torch_load_from_payload(res_post.content)
                edge_output = res_unpacked["data"]
                benchmark = [edge_output[i]["bench"] for i in range(len(edge_output))][
                    0
                ]
                latency_by_layer = [b["time"] for b in benchmark]

        else:
            output = net(input_dummy, local=local, split=split)
            latency_by_layer = [b["time"] for b in net.benchmark[-1]]

        latency_per_run.append(np.array(latency_by_layer))

    latency_avg = (np.sum(latency_per_run, axis=0) / repeat_times).tolist()

    return latency_avg


def get_layer_output_bytes(report: ModelStatistics) -> List[int]:
    sizes_by_layer = [m.output_bytes for m in report.summary_list if m.executed]
    return sizes_by_layer


def find_best_split(
    net: nn.Module,
    t_mobile: List[float],
    t_edge: List[float],
    t_ping: List[float],
    size_list: List[float],
    bandwidth,
) -> int:
    s, split = 1, 0
    best = 0

    for (t_m, t_e, size) in zip(t_mobile, t_edge, size_list):
        # For each module, evaluate client-transmission-server latency when
        # splitting network at this point
        t_mobile_acc = sum(t_mobile[:s])
        t_edge_acc = sum(t_edge[s:])

        e2e_latency = (
            t_mobile_acc + t_edge_acc + t_ping + SIZE_FACTOR * size * 8 / bandwidth
        )

        print(e2e_latency)

        if e2e_latency < best or best == 0:
            split = s
            best = e2e_latency

        s += 1

    return split


def plot_profile(t_mobile, t_edge, size_by_layer):
    import matplotlib.pyplot as plt
    import numpy as np
    import matplotlib

    plt.rcParams["figure.autolayout"] = True

    font = {"family": "normal", "weight": "regular", "size": 18}

    matplotlib.rc("font", **font)

    layers = np.arange(len(size_by_layer))

    def plot_sizes():
        plt.figure(figsize=[12, 6])
        plt.bar(
            layers,
            size_by_layer,
            color="green",
            alpha=0.6,
            align="edge",
            tick_label=layers,
        )
        plt.axis([-1, len(layers), 0, 1.1 * max(size_by_layer)])
        plt.grid(True)
        plt.minorticks_on()
        plt.xlabel("Layer")
        plt.ylabel("Checkpoint Size (MB)")

        plt.savefig("./plots/plot-sizes.png")

    def plot_inf_time():
        with open("log/bench-rpi-29.json") as j:
            json_rpi = json.load(j)
            t_rpi = [m["time"] for m in json_rpi["benchmark"][0]]

        width = 0.25

        plt.figure(figsize=[12, 6])
        plt.bar(
            layers - width,
            t_mobile,
            width=width,
            color="blue",
            alpha=0.6,
            label="Laptop",
            tick_label=layers,
        )

        plt.bar(
            layers,
            t_rpi,
            width=width,
            color="red",
            alpha=0.6,
            label="Raspberry Pi",
            tick_label=layers,
        )
        plt.bar(
            layers + width,
            t_edge,
            width=width,
            color="green",
            alpha=0.6,
            label="Edge",
            tick_label=layers,
        )
        plt.legend()
        # plt.axis([-1, len(layers), 0, 1.1 * max(np.maximum(t_mobile, t_edge))])
        plt.grid(True)
        plt.xlabel("Layer")
        plt.ylabel("Inference Time (ms, Log Scale)")
        plt.yscale("log")

        plt.savefig("./plots/plot-inference-time.png")
        # plt.show()

    def plot_all_splits(path: str, device: str = "", total_splits=30):
        with open(path, "r") as f:
            benchmark = json.load(f)

        t_inference = [split["time_full"] for split in benchmark]

        width = 0.4

        plt.figure(figsize=[12, 6])
        plt.bar(
            layers,
            t_inference,
            width=width,
            color="blue",
            alpha=0.6,
            label=device,
            tick_label=layers,
        )

        plt.legend()
        # plt.axis([-1, len(layers), 0, 1.1 * max(np.maximum(t_mobile, t_edge))])
        plt.grid(True)
        plt.xlabel("Layer")
        plt.ylabel("Inference Time (ms, Log Scale)")
        plt.yscale("log")

        plt.savefig(f"./plots/plot-inference-splits-{device}.png")
        # plt.show()

    def plot_all_splits_multi(paths, scenarios, device=""):
        t_list = []
        for p in paths:
            with open(p, "r") as f:
                benchmark = json.load(f)
                t_inference = [split["time_full"] for split in benchmark]
                t_list.append(t_inference)

        width = 0.25

        plt.figure(figsize=[12, 6])

        colors = ["blue", "red", "green"]
        for s in range(len(t_list)):
            plt.bar(
                layers + s * width,
                t_list[s],
                width=width,
                color=colors[s],
                alpha=0.6,
                label=scenarios[s],
                tick_label=layers,
            )

        plt.legend()
        # plt.axis([-1, len(layers), 0, 1.1 * max(np.maximum(t_mobile, t_edge))])
        plt.grid(True)
        plt.xlabel("Layer")
        plt.ylabel("Inference Time (ms, Log Scale)")
        plt.yscale("log")

        plt.savefig(f"./plots/plot-inference-splits-{device}-{scenarios}.png")

    def plot_video(path, scenario=""):
        with open(path) as f:
            buf = f.readlines()
            buf = [float(line.rstrip()) for line in buf if line != "\n"]

        width = 0.25
        backbone_layers = np.arange(len(buf))

        plt.figure(figsize=[12, 6])
        plt.bar(
            backbone_layers,
            buf,
            width=width,
            color="blue",
            alpha=0.6,
            label="Laptop",
            tick_label=backbone_layers,
        )
        plt.legend()
        plt.axis([-1, len(layers), 0, 1.1 * max(buf)])
        plt.grid(True)
        plt.xlabel("Layer")
        plt.ylabel("Video Detection FPS (ms, Log Scale)")
        # plt.yscale("log")

        plt.savefig(f"./plots/plot-inference-video-laptop{scenario}.png")

    plot_sizes()
    plot_inf_time()
    plot_all_splits("./log/bench_all_layers_laptop_wifi.json", device="Laptop,WiFi")
    plot_all_splits("./log/bench_all_layers_laptop_wifi1.json", device="Laptop,WiFi1")
    plot_all_splits("./log/bench_all_layers_laptop_wifi2.json", device="Laptop,WiFi2")
    plot_all_splits("./log/bench_all_layers_laptop_wifi3.json", device="Laptop,WiFi3")
    plot_all_splits("./log/bench_all_layers_laptop_wifi4.json", device="Laptop,WiFi4")
    plot_all_splits("./log/bench_all_layers_laptop_wifi5.json", device="Laptop,WiFi5")
    plot_all_splits("./log/bench_all_layers_laptop_5g.json", device="Laptop,5G")
    plot_all_splits("./log/bench_all_layers_laptop_4g.json", device="Laptop,4G")
    plot_all_splits("./log/bench_all_layers_laptop_lan.json", device="Laptop,LAN")
    plot_all_splits(
        "./log/bench_all_layers_laptop-debug_lan1.json", device="Laptop,LAN1"
    )
    plot_all_splits(
        "./log/bench_all_layers_laptop-debug_lan2.json", device="Laptop,LAN2"
    )
    plot_all_splits(
        "./log/bench_all_layers_laptop-debug_lan3.json", device="Laptop,LAN3"
    )
    plot_all_splits(
        "./log/bench_all_layers_laptop-debug_lan4.json", device="Laptop,LAN4"
    )
    plot_all_splits(
        "./log/bench_all_layers_laptop-debug_lan5.json", device="Laptop,LAN5"
    )
    plot_all_splits("./log/bench_all_layers_rpi_wifi.json", device="RaspberryPi,WiFi")
    plot_all_splits("./log/bench_all_layers_rpi_5g.json", device="RaspberryPi,5G")
    plot_all_splits("./log/bench_all_layers_rpi_4g.json", device="RaspberryPi,4G")
    plot_all_splits("./log/bench_all_layers_rpi_lan.json", device="RaspberryPi,LAN")
    plot_all_splits_multi(
        [
            "./log/bench_all_layers_laptop_4g.json",
            "./log/bench_all_layers_laptop_5g.json",
            "./log/bench_all_layers_laptop_wifi.json",
        ],
        ["4g", "5g", "wifi"],
        device="laptop",
    )
    plot_all_splits_multi(
        [
            "./log/bench_all_layers_rpi_4g.json",
            "./log/bench_all_layers_rpi_5g.json",
            "./log/bench_all_layers_rpi_wifi.json",
        ],
        ["4g", "5g", "wifi"],
        device="rpi",
    )
    plot_video("./log/bench_video_laptop.txt")
    plot_video("./log/bench_video_laptop_lan.txt", scenario="lan")

    # plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("target", type=str, default="profile")

    torch.set_num_threads(1)

    # report_torchinfo = summary_ssd_vgg16()
    report_torchinfo = summary_vgg11()
    # executed_modules = [m for m in report_torchinfo.summary_list if m.executed]

    t_mobile = test_inference(
        vgg_model, shape=(1, 3, 224, 224), local=True, repeat_times=10
    )
    t_edge = test_inference(
        vgg_model, shape=(1, 3, 224, 224), local=False, repeat_times=10
    )
    size_by_layer = get_layer_output_bytes(report_torchinfo)

    args = parser.parse_args()
    print(args)
    if args.target == "plot":
        plot_profile(t_mobile, t_edge, size_by_layer)

    else:
        t_ping = test_latency(HOST, dry_run=True)
        print(f"Ping: {t_ping}")
        bandwidth = test_bandwidth(HOST, dry_run=True)
        print(f"Bandwidth: {bandwidth}")

        split = find_best_split(
            vgg_model, t_mobile, t_edge, t_ping, size_by_layer, bandwidth
        )

        print("Best split is at " + str(split))

        with open("profile_network.json", "w") as f:
            data = json.dumps(
                {
                    "time": str(datetime.datetime.now()),
                    "ping": t_ping,
                    "bandwidth": bandwidth,
                    "best_split": split,
                }
            )
            f.write(data)

        with open("profile_inference.json", "w") as f:
            f.write(str(report_torchinfo))

        pass
