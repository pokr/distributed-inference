from io import BytesIO
import os, imghdr
import torch


def get_net_funcs(net: str):
    """
    Returns a tuple of network-specific functions

    Returns:
        (net_inference_run, net_classifier, net_model)

    """
    if net == "vgg":
        from vgg.net import (
            vgg_model,
            vgg_run_inference,
            vgg_generate_input,
            vgg_classify,
        )

        return (vgg_run_inference, vgg_generate_input, vgg_classify, vgg_model)
    elif net == "alexnet":
        from alexnet.net import (
            alex_model,
            alex_run_inference,
            alex_generate_input,
            alex_classify,
        )

        return (alex_run_inference, alex_generate_input, alex_classify, alex_model)
    else:
        raise ValueError("Unsupported network! ")


def tensor_to_list(tensor):
    return tensor.tolist()


def generate_test_input(net_type: str, filename: str = "vgg/dog.jpg"):
    if net_type == "vgg":
        from PIL import Image
        from torchvision import transforms

        input_image = Image.open(filename)
        preprocess = transforms.Compose(
            [
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
                transforms.Normalize(
                    mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
                ),
            ]
        )
        input_tensor = preprocess(input_image)
        input_batch = input_tensor.unsqueeze(0)
        return input_batch


def scan_images(path: str):
    file_list = []
    if os.path.isdir(path):
        for file in os.scandir(path):
            if imghdr.what(file) != "":  # File is an image
                file_list.append(file.path)
    else:  # Path points to a single file
        file_list.append(path)

    return file_list


def torch_load_from_payload(payload_bytes: bytes, device: str = "cpu"):
    buf = BytesIO()
    buf.write(payload_bytes)
    buf.seek(0)

    return torch.load(buf, map_location=device)


def torch_save_to_bytes(tensor_save: torch.Tensor):
    buf = BytesIO()
    torch.save(tensor_save, buf)
    buf.seek(0)

    return buf
