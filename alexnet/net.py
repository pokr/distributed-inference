from typing import Any
from click import progressbar
import torch
import torch.nn as nn
from torchvision import transforms
from torchvision.models import AlexNet
from PIL import Image


class AlexNetCustom(AlexNet):
    def __init__(self, num_classes: int = 1000, dropout: float = 0.5) -> None:
        super().__init__(num_classes, dropout)

    def convert_module_list(self):
        self.module_list = nn.ModuleList()
        for layer_f in self.features:
            self.module_list.append(layer_f)
        self.module_list.append(self.avgpool)
        self.module_list.append(nn.Flatten(start_dim=1))
        for layer_c in self.classifier:
            self.module_list.append(layer_c)

    def forward(self, x: torch.Tensor, split: int, local: bool) -> torch.Tensor:
        if local:  # Local
            for i in range(0, split):
                starter = torch.cuda.Event(enable_timing=True)
                ender = torch.cuda.Event(enable_timing=True)

                starter.record()
                x = self.module_list[i](x)
                ender.record()

                torch.cuda.synchronize()
                time_cost = starter.elapsed_time(ender)
                print(f"Module #{i}: {self.module_list[i]}")
                print(f"Time: {time_cost}")
                print(f"Shape: {x.shape} - {x.nelement() * x.element_size()} Bytes")

        if not local:  # Edge
            for i in range(split, len(self.module_list)):
                x = self.module_list[i](x)

        return x


def _alexnet_custom(
    pretrained: bool = False, progress: bool = True, **kwargs: Any
) -> AlexNet:
    model = AlexNetCustom(**kwargs)
    if pretrained:
        state_dict = torch.load("checkpoints/alexnet-owt-7be5be79.pth")
        model.load_state_dict(state_dict)
        model.convert_module_list()

    return model


alex_model = _alexnet_custom(pretrained=True, progress=True)
alex_model.eval()


def alex_generate_input(filename):
    input_image = Image.open(filename)
    preprocess = transforms.Compose(
        [
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ]
    )
    input_tensor = preprocess(input_image)
    input_batch = input_tensor.unsqueeze(0)

    return input_batch


def alex_run_inference(input_batch, local: bool, split: int):
    # move the input and model to GPU for speed if available
    if torch.cuda.is_available():
        input_batch = input_batch.to("cuda")
        alex_model.to("cuda")

    with torch.no_grad():
        output = alex_model(input_batch, local=local, split=split)
    # Tensor of shape 1000, with confidence scores over Imagenet's 1000 classes
    # print(output[0])
    # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
    # probabilities = torch.nn.functional.softmax(output[0], dim=0)
    # print(probabilities)

    return output


def alex_classify(output):
    # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
    probabilities = torch.nn.functional.softmax(output[0], dim=0)
    # print(probabilities)

    # Read the categories
    with open("imagenet_classes.txt", "r") as f:
        categories = [s.strip() for s in f.readlines()]
    # Show top categories per image
    top5_prob, top5_catid = torch.topk(probabilities, 5)

    result = {}
    for i in range(top5_prob.size(0)):
        result[categories[top5_catid[i]]] = top5_prob[i].item()

    return result
